//
//  BigCar.swift
//  Task_1_2_sw
//
//  Created by кмв on 19.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

import Foundation
class BigCar : BaseAuto
{
    var transp: Bool
    var masa: Int
    override init()
    {
        transp = false
        masa = 700
    }
    func moveObject(_ trsp: Bool) -> Void
    {
        transp = trsp
    }
    
    func objectWeight(_ masaa: Int) -> Int
    {
        if(transp == true)
        {
            masa = masaa
        }
        else
        {
            masa = 0
        }
        return (masa)
    }
}
