//
//  Car.swift
//  Task_1_2_sw
//
//  Created by кмв on 19.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

import Foundation
class Car : BaseAuto
{
    
    var pass: Bool
    var pName: String
    
    override init()
    {
        pass = false
        pName = "No pass"
    }
    func takePassenger(_ passenger: Bool) -> Void
    {
        pass = passenger
    }
    func passengerName(_ passName: String) -> String
    {
        if(pass == true)
        {
            pName = passName
        }
        else
        {
            pName = "no pass"
        }
        return (pName)
    }
}
