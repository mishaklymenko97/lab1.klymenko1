//
//  main.swift
//  Task_1_2_sw
//
//  Created by кмв on 19.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

import Foundation

var MyCar: BaseAuto = BaseAuto ()
MyCar.About(150,120)
print ("BaseAuto power = \(MyCar.power)")
print ("BaseAuto speed= \(MyCar.speed)")

var MCar: Car = Car()
MCar.takePassenger(true)
var pN = MCar.passengerName("misha")
print ("passenger name: \(MCar.pName)")
MCar.About(250, 103)
print ("Car speed= \(MCar.speed) Car power = \(MCar.power)")

var ca: BigCar = BigCar()
ca.About(300, 200)
print ("BigCar speed = \(ca.speed) BigCar power = \(ca.power)")
ca.moveObject(true)
var mass = ca.objectWeight(300)
print("Object weigh = \(mass) kg")

