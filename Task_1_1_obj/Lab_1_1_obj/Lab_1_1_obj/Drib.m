//
//  Drib.m
//  Lab_1_1_obj
//
//  Created by кмв on 19.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import "Drib.h"

@implementation Drib
-(id) init{
    Numerator=1;
    Denumerator=1;
    return self;
}

-(id) initNumerator:(int)numerator{
    Numerator=numerator;
    Denumerator=1;
    return self;
}

-(id)initNumerator:(int)numerator initDenumerator:(int)denumerator{
    Numerator=numerator;
    Denumerator=denumerator;
    return self;
}

-(void) add:(Drib *)drib{
    Numerator=Numerator*drib->Denumerator+drib->Numerator*Denumerator;
    Denumerator=Denumerator*drib->Denumerator;
    
}

-(void) sub:(Drib *)drib{
    Numerator=Numerator*drib->Denumerator-drib->Numerator*Denumerator;
    Denumerator=Denumerator*drib->Denumerator;
}

-(void) mul:(Drib *)drib{
    Numerator=Numerator*drib->Numerator;
    Denumerator=Denumerator*drib->Denumerator;
}

-(void) div:(Drib *)drib{
    Numerator=Numerator*drib->Denumerator;
    Denumerator=Denumerator*drib->Numerator;
}

-(int) findNSD: (int) num andDenum:(int) denum{
    if(num==0) return denum;
    while(denum!=0){
        int nsd=num%denum;
        num=denum;
        denum=nsd;
    }
    return num;
}

-(void) simplify{
    
    int nsd = [self findNSD:Numerator andDenum:Denumerator];
    Numerator=Numerator/nsd;
    Denumerator=Denumerator/nsd;
    
}

-(void) show{
    NSLog(@"| num is %d / denum is %d", Numerator, Denumerator);
    [self simplify];
    NSLog(@"| num is %d / denum is %d", Numerator, Denumerator);
}
@end
