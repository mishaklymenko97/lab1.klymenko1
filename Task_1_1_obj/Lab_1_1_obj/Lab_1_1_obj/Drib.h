//
//  Drib.h
//  Lab_1_1_obj
//
//  Created by кмв on 19.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Drib : NSObject
{
    int Numerator;
    int Denumerator;
}

-(id) init;
-(id) initNumerator: (int) numerator;
-(id) initNumerator: (int) numerator initDenumerator: (int) denumerator;
-(int) findNSD: (int) num andDenum:(int) denum;
-(void) add: (Drib*) drib;
-(void) sub: (Drib*) drib;
-(void) mul: (Drib*) drib;
-(void) div: (Drib*) drib;
-(void) show;
@end
