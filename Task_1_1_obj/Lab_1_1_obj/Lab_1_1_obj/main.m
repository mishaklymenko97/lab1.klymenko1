//
//  main.m
//  Lab_1_1_obj
//
//  Created by кмв on 19.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Drib.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Drib *one = [[Drib alloc] initNumerator:9 initDenumerator:6];
        Drib *two = [[Drib alloc] initNumerator:5 initDenumerator:6];
        [one add:two];
        [one show];
    }
    return 0;
}

