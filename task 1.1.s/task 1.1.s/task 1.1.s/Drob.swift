//
//  Drob.swift
//  task 1.1.s
//
//  Created by кмв on 16.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

import Foundation

class Drib {
    var numerator:Int
    var denumerator:Int
    
    init(){
        numerator=1
        denumerator=1
    }
    
    init(num:Int){
        numerator=num
        denumerator=1
    }
    
    
    init(num:Int, denum:Int){
        numerator=num
        denumerator=denum
    }
    
    func about(){
        print(" \(numerator) / \(denumerator)")
    }
    
    func add(secondDrib:Drib){
        numerator=numerator*secondDrib.denumerator+secondDrib.numerator*denumerator
        denumerator*=secondDrib.denumerator
        simplify()
    }
    
    func sub(secondDrib:Drib){
        numerator=numerator*secondDrib.denumerator-secondDrib.numerator*denumerator
        denumerator*=secondDrib.denumerator
        simplify()
    }
    
    func mul(secondDrib:Drib){
        numerator*=secondDrib.numerator
        denumerator*=secondDrib.denumerator
        simplify()
    }
    
    
    func div(secondDrib:Drib){
        numerator*=secondDrib.denumerator
        denumerator*=secondDrib.numerator
        simplify()
    }
    
    private func NSD(num:Int, denum:Int)->Int{
        var _denum=denum
        var _num=num
        if(_num==0){ return _denum}
        while(_denum != 0){
            var nsd:Int
            nsd=_num%_denum
            _num = _denum
            _denum = nsd
        }
        return _num
    }
    
    private func simplify(){
        let nsd = NSD(num: numerator, denum: denumerator)
        numerator=numerator/nsd
        denumerator=denumerator/nsd
    }
}
