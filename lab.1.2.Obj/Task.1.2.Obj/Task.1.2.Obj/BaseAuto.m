//
//  BaseAuto.m
//  Task.1.2.Obj
//
//  Created by кмв on 17.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import "BaseAuto.h"
@implementation BaseAuto

- (void)About:(NSInteger)Spd andPw :(NSInteger)Pw {
    _speed = Spd;
    _power = Pw;
    NSLog(@"--------------------------------");
    NSLog(@" | power =  %li; speed = %li", _power, _speed);
}

@end
