//
//  main.m
//  Task.1.2.Obj
//
//  Created by кмв on 17.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseAuto.h"
#import "Car.h"
#import "BigCar.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        BaseAuto *mcar = [[BaseAuto alloc] init];
        [mcar About:250 andPw:130];
        
        Car *car = [[Car alloc] init];
        [car About:180 andPw:100];
        [car takePassenger:false];
        NSLog(@"| passengerName = %@", [car passengerName:@"Misha"]);
        
        BigCar *bcar = [[BigCar alloc] init];
        [bcar About:80 andPw:50];
        [bcar moveObject:false];
        NSLog(@"| moveObject = %li kg", [bcar objectWeight:500] );

    }
    return 0;
}
