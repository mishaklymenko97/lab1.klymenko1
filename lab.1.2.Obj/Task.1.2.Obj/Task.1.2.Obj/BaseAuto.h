//
//  BaseAuto.h
//  Task.1.2.Obj
//
//  Created by кмв on 17.09.17.
//  Copyright © 2017 кмв. All rights reserved.

#import <Foundation/Foundation.h>
#ifndef BaseAuto_h
#define BaseAuto_h

@interface BaseAuto : NSObject
{
    NSInteger _speed;
    NSInteger _power;
}
- (void)About:(NSInteger)Spd andPw :(NSInteger)Pw;
@end

#endif /* BaseAuto_h */
