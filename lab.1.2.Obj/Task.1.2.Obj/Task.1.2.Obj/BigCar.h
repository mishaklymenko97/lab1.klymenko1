//
//  BigCar.h
//  Task.1.2.Obj
//
//  Created by кмв on 17.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import "BaseAuto.h"

@interface BigCar : BaseAuto
{
    Boolean _transp;
    NSInteger _masa;
}
-(void) moveObject: (Boolean) transp;
-(NSInteger) objectWeight: (NSInteger) masa;

@end
