//
//  Car.m
//  Task.1.2.Obj
//
//  Created by кмв on 17.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import "Car.h"

@implementation Car
-(void) takePassenger: (Boolean) passenger{
    _pass = passenger;
}
-(NSString *) passengerName: (NSString *) passName
{
    if (_pass == true)
    {    _pName = [[NSString alloc] initWithString:passName];
        return _pName;}
    else {
        return @"No passenger";
    }
}
@end
