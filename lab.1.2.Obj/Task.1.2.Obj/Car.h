//
//  Car.h
//  Task.1.2.Obj
//
//  Created by кмв on 17.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

#import "BaseAuto.h"

@interface Car : BaseAuto
{
    Boolean _pass;
    NSString *_pName;
}
-(void) takePassenger: (Boolean) passenger;
-(NSString *) passengerName: (NSString *) passName;
@end
